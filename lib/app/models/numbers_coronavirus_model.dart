class NumbersCoronaVirusModel {
  int cases;
  int deaths;
  int recovered;

  NumbersCoronaVirusModel({this.cases, this.deaths, this.recovered});

  factory NumbersCoronaVirusModel.fromJson(Map<String, dynamic> json) {
    return NumbersCoronaVirusModel(
        cases: json["cases"],
        deaths: json["deaths"],
        recovered: json["recovered"]);
  }

  Map<String, dynamic> toJson(NumbersCoronaVirusModel model) {
    Map<String, dynamic> json = Map<String, dynamic>();
    json = {
      "cases": model.cases,
      "deaths": model.deaths,
      "recovered": model.recovered,
    };

    return json;
  }
}
