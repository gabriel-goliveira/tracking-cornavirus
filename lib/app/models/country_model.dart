class CountryModel {
  final String country;
  final String iso2;
  final String flag;
  final int cases;
  final int todayCases;
  final int deaths;
  final int todayDeaths;
  final int recovered;
  final int active;
  final int critical;
  CountryModel(
      {this.country,
      this.iso2,
      this.flag,
      this.cases,
      this.todayCases,
      this.deaths,
      this.todayDeaths,
      this.recovered,
      this.active,
      this.critical});

  factory CountryModel.fromJson(Map<String, dynamic> json) {
    return CountryModel(
      country: json["country"],
      iso2: json["countryInfo"]["iso2"],
      flag: json["countryInfo"]["flag"],
      cases: json["cases"],
      todayCases: json["todayCases"],
      deaths: json["deaths"],
      todayDeaths: json["todayDeaths"],
      recovered: json["recovered"],
      active: json["active"],
      critical: json["critical"],
    );
  }

  Map<String, dynamic> toJson(CountryModel model) {
    Map<String, dynamic> json = Map<String, dynamic>();

    return json;
  }
}
