// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AppController on _AppBase, Store {
  final _$brazilAtom = Atom(name: '_AppBase.brazil');

  @override
  CountryModel get brazil {
    _$brazilAtom.context.enforceReadPolicy(_$brazilAtom);
    _$brazilAtom.reportObserved();
    return super.brazil;
  }

  @override
  set brazil(CountryModel value) {
    _$brazilAtom.context.conditionallyRunInAction(() {
      super.brazil = value;
      _$brazilAtom.reportChanged();
    }, _$brazilAtom, name: '${_$brazilAtom.name}_set');
  }

  final _$chinaAtom = Atom(name: '_AppBase.china');

  @override
  CountryModel get china {
    _$chinaAtom.context.enforceReadPolicy(_$chinaAtom);
    _$chinaAtom.reportObserved();
    return super.china;
  }

  @override
  set china(CountryModel value) {
    _$chinaAtom.context.conditionallyRunInAction(() {
      super.china = value;
      _$chinaAtom.reportChanged();
    }, _$chinaAtom, name: '${_$chinaAtom.name}_set');
  }

  final _$usaAtom = Atom(name: '_AppBase.usa');

  @override
  CountryModel get usa {
    _$usaAtom.context.enforceReadPolicy(_$usaAtom);
    _$usaAtom.reportObserved();
    return super.usa;
  }

  @override
  set usa(CountryModel value) {
    _$usaAtom.context.conditionallyRunInAction(() {
      super.usa = value;
      _$usaAtom.reportChanged();
    }, _$usaAtom, name: '${_$usaAtom.name}_set');
  }

  final _$italyAtom = Atom(name: '_AppBase.italy');

  @override
  CountryModel get italy {
    _$italyAtom.context.enforceReadPolicy(_$italyAtom);
    _$italyAtom.reportObserved();
    return super.italy;
  }

  @override
  set italy(CountryModel value) {
    _$italyAtom.context.conditionallyRunInAction(() {
      super.italy = value;
      _$italyAtom.reportChanged();
    }, _$italyAtom, name: '${_$italyAtom.name}_set');
  }

  final _$irelandAtom = Atom(name: '_AppBase.ireland');

  @override
  CountryModel get ireland {
    _$irelandAtom.context.enforceReadPolicy(_$irelandAtom);
    _$irelandAtom.reportObserved();
    return super.ireland;
  }

  @override
  set ireland(CountryModel value) {
    _$irelandAtom.context.conditionallyRunInAction(() {
      super.ireland = value;
      _$irelandAtom.reportChanged();
    }, _$irelandAtom, name: '${_$irelandAtom.name}_set');
  }

  final _$portugalAtom = Atom(name: '_AppBase.portugal');

  @override
  CountryModel get portugal {
    _$portugalAtom.context.enforceReadPolicy(_$portugalAtom);
    _$portugalAtom.reportObserved();
    return super.portugal;
  }

  @override
  set portugal(CountryModel value) {
    _$portugalAtom.context.conditionallyRunInAction(() {
      super.portugal = value;
      _$portugalAtom.reportChanged();
    }, _$portugalAtom, name: '${_$portugalAtom.name}_set');
  }

  final _$spainAtom = Atom(name: '_AppBase.spain');

  @override
  CountryModel get spain {
    _$spainAtom.context.enforceReadPolicy(_$spainAtom);
    _$spainAtom.reportObserved();
    return super.spain;
  }

  @override
  set spain(CountryModel value) {
    _$spainAtom.context.conditionallyRunInAction(() {
      super.spain = value;
      _$spainAtom.reportChanged();
    }, _$spainAtom, name: '${_$spainAtom.name}_set');
  }

  final _$argentinaAtom = Atom(name: '_AppBase.argentina');

  @override
  CountryModel get argentina {
    _$argentinaAtom.context.enforceReadPolicy(_$argentinaAtom);
    _$argentinaAtom.reportObserved();
    return super.argentina;
  }

  @override
  set argentina(CountryModel value) {
    _$argentinaAtom.context.conditionallyRunInAction(() {
      super.argentina = value;
      _$argentinaAtom.reportChanged();
    }, _$argentinaAtom, name: '${_$argentinaAtom.name}_set');
  }

  final _$germanyAtom = Atom(name: '_AppBase.germany');

  @override
  CountryModel get germany {
    _$germanyAtom.context.enforceReadPolicy(_$germanyAtom);
    _$germanyAtom.reportObserved();
    return super.germany;
  }

  @override
  set germany(CountryModel value) {
    _$germanyAtom.context.conditionallyRunInAction(() {
      super.germany = value;
      _$germanyAtom.reportChanged();
    }, _$germanyAtom, name: '${_$germanyAtom.name}_set');
  }

  final _$japanAtom = Atom(name: '_AppBase.japan');

  @override
  CountryModel get japan {
    _$japanAtom.context.enforceReadPolicy(_$japanAtom);
    _$japanAtom.reportObserved();
    return super.japan;
  }

  @override
  set japan(CountryModel value) {
    _$japanAtom.context.conditionallyRunInAction(() {
      super.japan = value;
      _$japanAtom.reportChanged();
    }, _$japanAtom, name: '${_$japanAtom.name}_set');
  }

  final _$mexicoAtom = Atom(name: '_AppBase.mexico');

  @override
  CountryModel get mexico {
    _$mexicoAtom.context.enforceReadPolicy(_$mexicoAtom);
    _$mexicoAtom.reportObserved();
    return super.mexico;
  }

  @override
  set mexico(CountryModel value) {
    _$mexicoAtom.context.conditionallyRunInAction(() {
      super.mexico = value;
      _$mexicoAtom.reportChanged();
    }, _$mexicoAtom, name: '${_$mexicoAtom.name}_set');
  }

  final _$canadaAtom = Atom(name: '_AppBase.canada');

  @override
  CountryModel get canada {
    _$canadaAtom.context.enforceReadPolicy(_$canadaAtom);
    _$canadaAtom.reportObserved();
    return super.canada;
  }

  @override
  set canada(CountryModel value) {
    _$canadaAtom.context.conditionallyRunInAction(() {
      super.canada = value;
      _$canadaAtom.reportChanged();
    }, _$canadaAtom, name: '${_$canadaAtom.name}_set');
  }

  final _$indiaAtom = Atom(name: '_AppBase.india');

  @override
  CountryModel get india {
    _$indiaAtom.context.enforceReadPolicy(_$indiaAtom);
    _$indiaAtom.reportObserved();
    return super.india;
  }

  @override
  set india(CountryModel value) {
    _$indiaAtom.context.conditionallyRunInAction(() {
      super.india = value;
      _$indiaAtom.reportChanged();
    }, _$indiaAtom, name: '${_$indiaAtom.name}_set');
  }

  final _$russiaAtom = Atom(name: '_AppBase.russia');

  @override
  CountryModel get russia {
    _$russiaAtom.context.enforceReadPolicy(_$russiaAtom);
    _$russiaAtom.reportObserved();
    return super.russia;
  }

  @override
  set russia(CountryModel value) {
    _$russiaAtom.context.conditionallyRunInAction(() {
      super.russia = value;
      _$russiaAtom.reportChanged();
    }, _$russiaAtom, name: '${_$russiaAtom.name}_set');
  }

  final _$singaporeAtom = Atom(name: '_AppBase.singapore');

  @override
  CountryModel get singapore {
    _$singaporeAtom.context.enforceReadPolicy(_$singaporeAtom);
    _$singaporeAtom.reportObserved();
    return super.singapore;
  }

  @override
  set singapore(CountryModel value) {
    _$singaporeAtom.context.conditionallyRunInAction(() {
      super.singapore = value;
      _$singaporeAtom.reportChanged();
    }, _$singaporeAtom, name: '${_$singaporeAtom.name}_set');
  }

  final _$loadingAtom = Atom(name: '_AppBase.loading');

  @override
  bool get loading {
    _$loadingAtom.context.enforceReadPolicy(_$loadingAtom);
    _$loadingAtom.reportObserved();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.context.conditionallyRunInAction(() {
      super.loading = value;
      _$loadingAtom.reportChanged();
    }, _$loadingAtom, name: '${_$loadingAtom.name}_set');
  }

  final _$numbersModelAtom = Atom(name: '_AppBase.numbersModel');

  @override
  NumbersCoronaVirusModel get numbersModel {
    _$numbersModelAtom.context.enforceReadPolicy(_$numbersModelAtom);
    _$numbersModelAtom.reportObserved();
    return super.numbersModel;
  }

  @override
  set numbersModel(NumbersCoronaVirusModel value) {
    _$numbersModelAtom.context.conditionallyRunInAction(() {
      super.numbersModel = value;
      _$numbersModelAtom.reportChanged();
    }, _$numbersModelAtom, name: '${_$numbersModelAtom.name}_set');
  }

  final _$countriesAtom = Atom(name: '_AppBase.countries');

  @override
  ObservableList<CountryModel> get countries {
    _$countriesAtom.context.enforceReadPolicy(_$countriesAtom);
    _$countriesAtom.reportObserved();
    return super.countries;
  }

  @override
  set countries(ObservableList<CountryModel> value) {
    _$countriesAtom.context.conditionallyRunInAction(() {
      super.countries = value;
      _$countriesAtom.reportChanged();
    }, _$countriesAtom, name: '${_$countriesAtom.name}_set');
  }

  final _$getInformationsAsyncAction = AsyncAction('getInformations');

  @override
  Future getInformations() {
    return _$getInformationsAsyncAction.run(() => super.getInformations());
  }

  @override
  String toString() {
    final string =
        'brazil: ${brazil.toString()},china: ${china.toString()},usa: ${usa.toString()},italy: ${italy.toString()},ireland: ${ireland.toString()},portugal: ${portugal.toString()},spain: ${spain.toString()},argentina: ${argentina.toString()},germany: ${germany.toString()},japan: ${japan.toString()},mexico: ${mexico.toString()},canada: ${canada.toString()},india: ${india.toString()},russia: ${russia.toString()},singapore: ${singapore.toString()},loading: ${loading.toString()},numbersModel: ${numbersModel.toString()},countries: ${countries.toString()}';
    return '{$string}';
  }
}
