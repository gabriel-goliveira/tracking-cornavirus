import 'package:mobx/mobx.dart';
import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:trackingcoronaviruscases/app/models/country_model.dart';
import 'models/numbers_coronavirus_model.dart';

part 'app_controller.g.dart';

class AppController = _AppBase with _$AppController;

abstract class _AppBase with Store {
  _AppBase() {
    getInformations();
  }

  static const String numbersUrl = "https://corona.lmao.ninja/all";
  static const String brazilUrl = "https://corona.lmao.ninja/countries/brazil";
  static const String chinaUrl = "https://corona.lmao.ninja/countries/China";
  static const String usaUrl = "https://corona.lmao.ninja/countries/USA";
  static const String italyUrl = "https://corona.lmao.ninja/countries/Italy";
  static const String irelandUrl =
      "https://corona.lmao.ninja/countries/Ireland";
  static const String portugalUrl =
      "https://corona.lmao.ninja/countries/Portugal";
  static const String spainUrl = "https://corona.lmao.ninja/countries/Spain";
  static const String argentinaUrl =
      "https://corona.lmao.ninja/countries/Argentina";
  static const String germanyUrl =
      "https://corona.lmao.ninja/countries/Germany";
  static const String japanUrl = "https://corona.lmao.ninja/countries/Japan";
  static const String mexicoUrl = "https://corona.lmao.ninja/countries/Mexico";
  static const String canadaUrl = "https://corona.lmao.ninja/countries/Canada";
  static const String indiaUrl = "https://corona.lmao.ninja/countries/India";
  static const String russiaUrl = "https://corona.lmao.ninja/countries/Russia";
  static const String singaporeUrl =
      "https://corona.lmao.ninja/countries/Singapore";

  @observable
  CountryModel brazil;
  @observable
  CountryModel china;
  @observable
  CountryModel usa;
  @observable
  CountryModel italy;
  @observable
  CountryModel ireland;
  @observable
  CountryModel portugal;
  @observable
  CountryModel spain;
  @observable
  CountryModel argentina;
  @observable
  CountryModel germany;
  @observable
  CountryModel japan;
  @observable
  CountryModel mexico;
  @observable
  CountryModel canada;
  @observable
  CountryModel india;
  @observable
  CountryModel russia;
  @observable
  CountryModel singapore;

  @observable
  bool loading = false;

  @observable
  NumbersCoronaVirusModel numbersModel;

  @observable
  ObservableList<CountryModel> countries = ObservableList<CountryModel>();

  @action
  getInformations() async {
    loading = true;
    try {
      var res = await http.get(Uri.encodeFull(numbersUrl),
          headers: {"Accept": "application/json"});
      numbersModel = NumbersCoronaVirusModel.fromJson(json.decode(res.body));
      var resBrazil = await http.get(Uri.encodeFull(brazilUrl),
          headers: {"Accept": "application/json"});
      brazil = CountryModel.fromJson(json.decode(resBrazil.body));
      var resChina = await http.get(Uri.encodeFull(chinaUrl),
          headers: {"Accept": "application/json"});
      china = CountryModel.fromJson(json.decode(resChina.body));
      var resUsa = await http
          .get(Uri.encodeFull(usaUrl), headers: {"Accept": "application/json"});
      usa = CountryModel.fromJson(json.decode(resUsa.body));
      var resItaly = await http.get(Uri.encodeFull(italyUrl),
          headers: {"Accept": "application/json"});
      italy = CountryModel.fromJson(json.decode(resItaly.body));
      var resIreland = await http.get(Uri.encodeFull(irelandUrl),
          headers: {"Accept": "application/json"});
      ireland = CountryModel.fromJson(json.decode(resIreland.body));
      var resPortugal = await http.get(Uri.encodeFull(portugalUrl),
          headers: {"Accept": "application/json"});
      portugal = CountryModel.fromJson(json.decode(resPortugal.body));
      var resSpain = await http.get(Uri.encodeFull(spainUrl),
          headers: {"Accept": "application/json"});
      spain = CountryModel.fromJson(json.decode(resSpain.body));
      var resArgentina = await http.get(Uri.encodeFull(argentinaUrl),
          headers: {"Accept": "application/json"});
      argentina = CountryModel.fromJson(json.decode(resArgentina.body));
      var resGermany = await http.get(Uri.encodeFull(germanyUrl),
          headers: {"Accept": "application/json"});
      germany = CountryModel.fromJson(json.decode(resGermany.body));

      var resJapan = await http.get(Uri.encodeFull(japanUrl),
          headers: {"Accept": "application/json"});
      japan = CountryModel.fromJson(json.decode(resJapan.body));
      var resMexico = await http.get(Uri.encodeFull(mexicoUrl),
          headers: {"Accept": "application/json"});
      mexico = CountryModel.fromJson(json.decode(resMexico.body));
      var resCanada = await http.get(Uri.encodeFull(canadaUrl),
          headers: {"Accept": "application/json"});
      canada = CountryModel.fromJson(json.decode(resCanada.body));
      var resIndia = await http.get(Uri.encodeFull(indiaUrl),
          headers: {"Accept": "application/json"});
      india = CountryModel.fromJson(json.decode(resIndia.body));
      var resRussia = await http.get(Uri.encodeFull(russiaUrl),
          headers: {"Accept": "application/json"});
      russia = CountryModel.fromJson(json.decode(resRussia.body));
      var resSingapore = await http.get(Uri.encodeFull(singaporeUrl),
          headers: {"Accept": "application/json"});
      singapore = CountryModel.fromJson(json.decode(resSingapore.body));
      countries.add(brazil);
      countries.add(china);
      countries.add(italy);
      countries.add(usa);
      countries.add(ireland);
      countries.add(portugal);
      countries.add(spain);
      countries.add(argentina);
      countries.add(germany);
      countries.add(japan);
      countries.add(mexico);
      countries.add(canada);
      countries.add(india);
      countries.add(russia);
    } catch (e) {
      throw e;
    } finally {
      loading = false;
    }
  }
}
