import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:trackingcoronaviruscases/app/modules/home/components/single_card_numbers/single_card_numbers_widget.dart';

class CardNumbersWidget extends StatelessWidget {
  final int cases;
  final int deaths;
  final int recovered;

  const CardNumbersWidget({Key key, this.cases, this.deaths, this.recovered})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 25,
      child: Neumorphic(
          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 15),
          boxShape: NeumorphicBoxShape.roundRect(
              borderRadius: BorderRadius.circular(12)),
          style: NeumorphicStyle(
              shape: NeumorphicShape.concave,
              depth: 8,
              lightSource: LightSource.topLeft,
              color: Colors.grey[300]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Números Atualizados",
                style: TextStyle(
                    color: Colors.grey[800],
                    fontWeight: FontWeight.bold,
                    fontSize: 24),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SingleCardNumbersWidget(
                    title: "Total \nde casos",
                    infos: cases.toString(),
                    color: Colors.yellow[900],
                  ),
                  SingleCardNumbersWidget(
                    title: "Total \nde mortos",
                    infos: deaths.toString(),
                    color: Colors.red[900],
                  ),
                  SingleCardNumbersWidget(
                    title: "Total de \ncurados",
                    infos: recovered.toString(),
                    color: Colors.blue[900],
                  )
                ],
              )
            ],
          )),
    );
  }
}
