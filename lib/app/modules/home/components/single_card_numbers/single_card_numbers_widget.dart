import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class SingleCardNumbersWidget extends StatelessWidget {
  final String title;
  final String infos;
  final Color color;

  const SingleCardNumbersWidget({Key key, this.title, this.infos, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 105,
      child: Neumorphic(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
        boxShape: NeumorphicBoxShape.roundRect(
            borderRadius: BorderRadius.circular(12)),
        style: NeumorphicStyle(
            shape: NeumorphicShape.concave,
            depth: 8,
            lightSource: LightSource.topLeft,
            color: Colors.grey[300]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.grey[800],
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              infos,
              style: TextStyle(fontSize: 15, color: color),
            ),
          ],
        ),
      ),
    );
  }
}
