import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:trackingcoronaviruscases/app/modules/home/components/card_country/card_country_widget.dart';

class CountryListWidget extends StatelessWidget {
  final countries;

  const CountryListWidget({Key key, this.countries}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      fit: FlexFit.loose,
      child: Container(
        width: MediaQuery.of(context).size.width - 25,
        child: Neumorphic(
          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 15),
          boxShape: NeumorphicBoxShape.roundRect(
              borderRadius: BorderRadius.circular(12)),
          style: NeumorphicStyle(
              shape: NeumorphicShape.concave,
              depth: 8,
              lightSource: LightSource.topLeft,
              color: Colors.grey[300]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Países",
                style: TextStyle(
                    color: Colors.grey[800],
                    fontWeight: FontWeight.bold,
                    fontSize: 24),
              ),
              SizedBox(
                height: 10,
              ),
              Flexible(
                fit: FlexFit.loose,
                child: ListView.builder(
                  itemCount: countries.length,
                  itemBuilder: (context, index) {
                    return CardCountryWidget(
                      country: countries[index],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
