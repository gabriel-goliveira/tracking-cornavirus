import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:trackingcoronaviruscases/app/modules/home/components/card_country/card_country_widget.dart';
import 'package:trackingcoronaviruscases/app/modules/home/components/card_numbers/card_numbers_widget.dart';
import 'package:trackingcoronaviruscases/app/modules/home/components/country_list/country_list_widget.dart';

import '../../app_controller.dart';
import '../../app_module.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final controller = AppModule.to.bloc<AppController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.greenAccent[700],
      appBar: AppBar(
        backgroundColor: Colors.greenAccent[700],
        elevation: 0,
      ),
      body: Observer(
        builder: (_) {
          return controller.loading == true
              ? Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  ),
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CardNumbersWidget(
                      cases: controller.numbersModel.cases ?? 0,
                      deaths: controller.numbersModel.deaths ?? 0,
                      recovered: controller.numbersModel.recovered ?? 0,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CountryListWidget(
                      countries: controller.countries,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(),
                  ],
                );
        },
      ),
    );
  }
}
