import 'package:flutter/material.dart';
import 'package:trackingcoronaviruscases/app/modules/home/home_module.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tracking Coronavirus',
      debugShowCheckedModeBanner: false,
      home: HomeModule(),
    );
  }
}
