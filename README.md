# Tracking Coronavirus #

Tracking Coronavirus is a Flutter app to track Covid-19 around the world. It was made for study purpose.

### What technologies were used?###

* Flutter
* Dart
* Mobx
* Slidy
* API
* Neumorphic Design

### What is the app prupose? ###

The app get updated informations about Covid-19 in some countries. In the future I pretend to show news about this countries. 

### Images ###

![picture](screenshots/screenshot.jpg)
