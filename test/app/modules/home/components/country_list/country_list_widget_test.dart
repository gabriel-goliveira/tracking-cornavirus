import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_pattern/bloc_pattern_test.dart';

import 'package:trackingcoronaviruscases/app/modules/home/components/country_list/country_list_widget.dart';

main() {
  testWidgets('CountryListWidget has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(CountryListWidget()));
    final textFinder = find.text('CountryList');
    expect(textFinder, findsOneWidget);
  });
}
