import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_pattern/bloc_pattern_test.dart';

import 'package:trackingcoronaviruscases/app/modules/home/components/card_country/card_country_widget.dart';

main() {
  testWidgets('CardCountryWidget has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(CardCountryWidget()));
    final textFinder = find.text('CardCountry');
    expect(textFinder, findsOneWidget);
  });
}
