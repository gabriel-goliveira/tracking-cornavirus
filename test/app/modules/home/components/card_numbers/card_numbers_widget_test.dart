import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_pattern/bloc_pattern_test.dart';

import 'package:trackingcoronaviruscases/app/modules/home/components/card_numbers/card_numbers_widget.dart';

main() {
  testWidgets('CardNumbersWidget has message', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(CardNumbersWidget()));
    final textFinder = find.text('CardNumbers');
    expect(textFinder, findsOneWidget);
  });
}
