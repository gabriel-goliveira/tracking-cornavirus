import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_pattern/bloc_pattern_test.dart';

import 'package:trackingcoronaviruscases/app/modules/home/components/single_card_numbers/single_card_numbers_widget.dart';

main() {
  testWidgets('SingleCardNumbersWidget has message',
      (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(SingleCardNumbersWidget()));
    final textFinder = find.text('SingleCardNumbers');
    expect(textFinder, findsOneWidget);
  });
}
